# var~cyan.docs.name~

var~description~

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'var~name.underscore~'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install var~name.underscore~

## Usage

## Development

After checking out the repo, run `bin/setup` to install dependencies. 

Then, run `rake unit` to run the unit tests.

To install this gem onto your local machine, run `rake install:local`. 
 at . This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

if~cyan.docs.contributing~	
## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.MD) for details on our code of conduct, and the process for submitting pull requests to us.
end~cyan.docs.contributing~	

if~cyan.docs.semver~
## Versioning 
We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.
end~cyan.docs.semver~

## Authors
* [var~cyan.docs.author~](mailto:var~cyan.docs.email~) 

if~cyan.docs.license~
## License
This project is licensed under var~cyan.docs.license~ - see the [LICENSE.md](LICENSE.MD) file for details
end~cyan.docs.license~