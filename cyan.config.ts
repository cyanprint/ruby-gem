import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, IAutoInquire, IAutoMapper, IExecute,} from "./Typings";
import {Kore, Core} from "@kirinnee/core"

let core: Core = new Kore();
core.ExtendPrimitives();

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
	
	
	let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
	let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);
	
	let variables: any = {
		name: {
			underscore: ["ruby_gem", "Enter name of your gem in underscore-case"]
		}
	};
	
	variables = await autoInquirer.InquireInput(variables);
	variables.name.pascal = (variables.name.underscore as string).split("_").Map(e => e.Capitalize()).join("");
	variables.name.author = docs.data.author;
	variables.email = docs.data.email;
	variables.description = docs.data.description;
	
	let flags: any = {
		rspec: {
			use: false,
			monkeypatch: false
		},
		minitest: false,
		ci: false
	};
	
	let test = await autoInquirer.InquirePredicate("Do you want to include Unit Test framework?");
	if (test) {
		let framework = await autoInquirer.InquirePredicate("Which test framework do you want to use?", "rspec", "Minitest");
		if (framework) {
			flags.rspec.use = true;
			flags.rspec.monkeypatch = await autoInquirer.InquirePredicate("Do you allow monkey patching in rSpec?");
		} else {
			flags.minitest = true;
		}
	}
	
	flags.ci = await autoInquirer.InquirePredicate("Do you want to include GitLab CI/CD yaml configure file to publish gems?");
	
	if (docs.usage.git) variables.git = docs.data.gitURL;
	
	return {
		globs: {root: "./Template", pattern: "**/*", ignore: "**/*.iml"},
		flags: flags,
		variable: variables,
		docs: docs,
		comments: ["#"]
	} as Cyan;
}
